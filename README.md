docker build -f Dockerfile-db -t service-db .
docker build -f Dockerfile-php -t service-phpmyadmin .
docker build -f Dockerfile-web -t service-web .



docker network create service-network
docker run --network=service-network --name service-db-container -d service-db
docker run --network=service-network --name service-phpmyadmin-container -p 8000:80 -d service-phpmyadmin
docker run --network=service-network --name service-web-container -p 8080:80 -d service-web



docker run --name service-db-container -d service-db
docker run --name service-phpmyadmin-container -p 8000:80 --link service-db-container:db -d service-phpmyadmin
docker run --name service-web-container -p 8080:80 --link service-db-container:db -d service-web